FROM openjdk:12
ADD target/docker-spring-boot-eduardoreis.jar docker-spring-boot-eduardoreis.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-eduardoreis.jar"]
